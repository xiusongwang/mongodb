#!/bin/bash


replaces='mongodb'
conflicts='mongodb'
pkgversion=1.0
pkgname=drainware-mongodb
pkggroup=drainware
pkgarch=amd64
maintainer=jose.palanco@drainware.com
requires='drainware-boost,monit'
command="scons --prefix=/opt/drainware --libpath=/opt/drainware/lib --usev8 --full --cpppath=/opt/drainware/include/ --sharedclient  install"




if [ $# -eq 1  ]
then
sudo checkinstall -y --replaces=$replaces --conflicts=$conflicts --nodoc --pkgversion=$pkgversion \
--pkgrelease=$1 --type=debian --pkgname=$pkgname --pkggroup=$pkggroup --pkgarch=$pkgarch \
--maintainer=$maintainer --requires=$requires --install=no --fstrans=no $command
mv *.deb ..


else
echo "usage: $0 [RELEASE]"
fi


