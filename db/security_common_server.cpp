// security_common.cpp
/*
 *    Copyright (C) 2010 10gen Inc.
 *
 *    This program is free software: you can redistribute it and/or  modify
 *    it under the terms of the GNU Affero General Public License, version 3,
 *    as published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This file contains inter-mongo instance security helpers.  Due to the
 * requirement that it be possible to compile this into mongos and mongod, it
 * should not depend on much external stuff.
 */

#include "pch.h"
#include "security.h"
#include "security_common_server.h"
#include "../client/dbclient.h"
#include "commands.h"
#include "nonce.h"
#include "../util/md5.hpp"

#include <sys/stat.h>

namespace mongo {

    bool noauth = true;

    bool AuthenticationInfo::_isAuthorized(const string& dbname, int level) const {
        {
            scoped_spinlock lk(_lock);

            if ( _isAuthorizedSingle_inlock( dbname , level ) )
                return true;

            if ( noauth )
                return true;

            if ( _isAuthorizedSingle_inlock( "admin" , level ) )
                return true;

            if ( _isAuthorizedSingle_inlock( "local" , level ) )
                return true;
        }
        return _isAuthorizedSpecialChecks( dbname );
    }

    bool AuthenticationInfo::_isAuthorizedSingle_inlock(const string& dbname, int level) const {
        MA::const_iterator i = _dbs.find(dbname);
        return i != _dbs.end() && i->second.level >= level;
    }

} // namespace mongo
